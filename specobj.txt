// specobj.txt - This very simple script waits until this object is searched. When it
// is, calls a given special node in the current town's script.

// Memory Cells - 
//   0 - Number of a state in the town script. This is the state that is called when the item is used.
//   1,2 - Coordinates for a stuff done flag. If these are 0 and 0, ignored. Otherwise,
//     is the given flag is non-zero, nothing happens when the object is used.

beginterrainscript; 

variables;

body;

beginstate INIT_STATE;

	break;

beginstate START_STATE;
break;

beginstate SEARCH_STATE;
	if ((get_memory_cell(1) > 0) || (get_memory_cell(2) > 0)) 
		if (get_flag(get_memory_cell(1),get_memory_cell(2)) > 0) {
			print_str_color("You can't do anything with this anymore.",2);
			end();
			}
	
	run_town_script(get_memory_cell(0));

break;

