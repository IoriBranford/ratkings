Merchant's environment:
high population = high demand for necessities and luxuries
manufacturing and trading centers + trade routes = lot of potential business
land rich in precious minerals = high money supply
advanced banking and financing systems for dealing with large quantities

Main roles in large trade organization:
Merchants finance and organize import/export
Carriers transport
Agents buy and sell

Traveling merchants' districts with various amenities:
apartments
storage facilities
places of worship
bath houses

Trading companies have:
transferrable shares
investors

Long-distance correspondence via couriers:
work under dedicated courier service companies, or directly for a specific trade org
carry mailbags on horseback
travel along set routes with regular rest stops
usually 3-6 days per 200 miles for delivery, with increased variance if sea travel involved
typical contents: commodity prices, exchange rates

Merchant's records:
transactions, accounts, supplies, customers, shareholders, etc.
require good education, thus boosting education industry which improved larger population's education level as a whole

Insurance protects value of goods in transit:
insurance broker spreads risk among several underwriters, in exchange for a premium equal to some percentage of the goods value

International banking:
International payment could be in bills of exchange rather than coin, bullion, etc if balance of trade was good enough

Local banking:
moneychangers started going into banking business
check = order to transfer from your account to another

Upper classes buy luxuries to mark themselves as above lower classes:
houses of stone or brick rather than mostly wood
fancy furniture, tapestries, frescoes, carpets, candles
expensive foods: meats, spices, game, exotic fish, wines
silverware, glassware
fashionable clothes from expensive materials (silk, satin, damask, velvet,
brocade, gold cloth, furs)
jewelry, art
