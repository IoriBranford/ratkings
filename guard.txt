// guard.txt
// A simple text for guards in towns. Acts like a regular npc unless it's 
// in a town party has angered, in which case it goes mobile and hunts party 
// down.

// Memory Cells:
//   Cell 0 - How creature moves.
//     0 - If 0, wander randomly. 
//     1 - Stands still until a target appears.
//     2 - Completely immobile, even if target appears.
//     3 - Walk to a waypoint and then back to start. Use Cell 4 to
//         indicate a waypoint.
//   Cell 1,2 - Stuff done flag. If both 0, nothing. Otherwise when this 
//     is killed, set to 1.
//   Cell 3 - Dialogue node to start with if talked to. if left at 0, this 
//     character doesn't talk.
//   Cell 4 - If Cell 0, is not 3, this value is ignore. Otherwise, it is 
//     the number of a waypoint. This guard spends its time walking to that
//     waypoint and back.

begincreaturescript;

variables;

short i,target;
short gone_to_waypoint = 0;

body;

beginstate INIT_STATE;
	if (get_memory_cell(0) == 2)
		set_mobility(ME,0);

	set_char_script_mode(ME,1); // act even at distance

	if (town_status(current_town()) == 2)
		set_mobility(ME,1);

	set_summon_level(ME,1);
	break;

beginstate DEAD_STATE;
	// Set the appropriate stuff done flag for this character being dead
	if ((get_memory_cell(1) != 0) || (get_memory_cell(2) != 0))
		set_flag(get_memory_cell(1),get_memory_cell(2),1);
break;

beginstate START_STATE; 
	// Has the town gone hostile?
	if (town_status(current_town()) == 2) {
		alert_char(ME);
		set_mobility(ME,1);
		}
		
	// if I have a target for some reason, go attack it
	if (target_ok()) {
		if (dist_to_char(get_target()) <= 16)
			set_state(3);
			else set_target(ME,-1);
		}
	
	// Look for a target, attack it if visible
	if (select_target(ME,8,0)) {
		do_attack();
		set_state(3);
		}
		
	// Have I been hit? Strike back!
	if (who_hit_me() >= 0) {
		set_target(ME,who_hit_me());
		do_attack();
		set_state(3);
		}
	
	// If we're hostile, hunt down party
	// Otherwise, just peacefully move around. Go back to start, if I'm too far
	// from where I started.
	if (get_attitude(ME) >= 10) 
		approach_char(ME,random_party_member(),6);
		else if ((get_memory_cell(0) != 3) && (my_dist_from_start() >= 6)) {
			if (get_ran(1,1,100) < 40) 
				return_to_start(ME,1);
			}
			else if (get_memory_cell(0) == 0) {
				fidget(ME,25);
				}
				else if (get_memory_cell(0) == 3) { 
					// march to waypoint and back
					if ((gone_to_waypoint == 0) && (get_ran(1,1,100) < 15)) {
						if (approach_waypoint(ME,get_memory_cell(4),1))
							gone_to_waypoint = 1;
						}
						else if ((gone_to_waypoint == 1) && (get_ran(1,1,100) < 15)) {
							if (return_to_start(ME,1))
								gone_to_waypoint = 0;
							}
					
					}

	// if we're in combat and the above didn't give me anything to do, just
	// stop now. Otherwise, game will keep running script, and that eats up CPU time.
	if (am_i_doing_action() == FALSE)
		end_combat_turn();
break;

beginstate 3; // attacking
	if (target_ok() == FALSE)
		set_state(START_STATE);
	do_attack();
break;

beginstate TALKING_STATE;
	if (get_memory_cell(3) == 0) {
		print_str("Talking: It doesn't respond.");
		end();
		}
	begin_talk_mode(get_memory_cell(3));
break;
